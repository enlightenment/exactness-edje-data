#include <Elementary.h>
#include <Evas.h>
#include <stdio.h>

static void
_my_win_del(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   elm_exit();
}

static void
_layout_file_set(Evas_Object *obj, const char *filename)
{
   char *group = NULL;
   if (edje_file_group_exists(filename, "main"))
     {
        group = strdup("main");
     }
   else
     {
        Eina_List *groups = edje_file_collection_list(filename);
        if (!groups)
          {
             fprintf(stderr, "ERROR: file '%s' has no groups!\n",
                   filename);
             return;
          }
        group = strdup(groups->data);

        edje_file_collection_list_free(groups);
     }

   elm_layout_file_set(obj, filename, group);
   free(group);
}

int
elm_main(int argc, char *argv[])
{
   Evas_Object *win, *obj, *bg;

   if (argc != 2)
     {
        fprintf(stderr, "Usage: ./elm_edje_player <filename>\n");
        return 1;
     }

   win = elm_win_add(NULL, "elm_edje_player", ELM_WIN_BASIC);
   elm_win_title_set(win, "elm_edje_player");
   elm_win_autodel_set(win, EINA_TRUE);
   evas_object_smart_callback_add(win, "delete,request", _my_win_del, NULL);

   bg = elm_bg_add(win);
   evas_object_size_hint_weight_set(bg, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   elm_win_resize_object_add(win, bg);
   evas_object_show(bg);

   obj = elm_layout_add(win);
   elm_win_resize_object_add(win, obj);
   evas_object_size_hint_weight_set(obj, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_show(obj);
   evas_object_show(win);

   _layout_file_set(obj, argv[1]);

     {
        Evas_Object *edje = elm_layout_edje_get(obj);
        Evas_Coord minw, minh;
        edje_object_size_min_get(edje, &minw, &minh);
        if ((minw <= 0) && (minh <= 0))
           edje_object_size_min_calc(edje, &minw, &minh);

        if (minw < 320) minw = 320;
        if (minh < 240) minh = 240;

        evas_object_size_hint_min_set(obj, minw, minh);
        evas_object_resize(obj, minw, minh);
        evas_object_resize(win, minw, minh);
     }

   elm_run();

   elm_shutdown();

   return 0;
}

ELM_MAIN()
